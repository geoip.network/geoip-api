import datetime
from cidr_man import CIDR
from cidr_man.cidr import PREFIX_UNION_T

from database import open_database
from local_cache import cache_this
from models import LookedUp, NotFound
from render_helpers import render_prefix


database = open_database()


@cache_this
def lookup_bulk(bulk):
    responses = []
    with database.read_txn() as txn, txn.cursor(
        database.prefix_db
    ) as prefix_curs, txn.cursor(database.route_db) as route_curs, txn.cursor(
        database.autnum_db
    ) as autnum_curs:
        for ip_or_cidr in bulk:
            try:
                requested_network: PREFIX_UNION_T = CIDR(ip_or_cidr)
                prefix, route, autnum = lookup_prefix_with_cursors(
                    requested_network, prefix_curs, route_curs, autnum_curs
                )
                if prefix is None:
                    result = {"error": "no covering prefix found"}
                else:
                    result = render_prefix(prefix, autnum)
            except ValueError:
                result = {"error": "invalid request"}
            responses.append(result)
    return responses


@cache_this
def lookup_prefix(requested_network: CIDR):
    with database.read_txn() as txn, txn.cursor(
        database.prefix_db
    ) as prefix_curs, txn.cursor(database.route_db) as route_curs, txn.cursor(
        database.autnum_db
    ) as autnum_curs:
        return lookup_prefix_with_cursors(
            requested_network, prefix_curs, route_curs, autnum_curs
        )


def lookup_prefix_with_cursors(
    requested_network: CIDR, prefix_cur, route_cur, autnum_cur
):
    network = requested_network
    prefix = None
    route = None
    autnum = None
    while network.prefixlen >= (8 if requested_network.version == 4 else 29):
        prefix = database.prefix_get_bulk(prefix_cur, network.compressed)
        if prefix is not None:
            break
        network = network.supernet()
    if prefix != None:
        route = database.route_get_by_prefix_bulk(route_cur, prefix.cidr.decode())
        autnum = database.autnum_get_bulk(autnum_cur, route.asn)
    return prefix, route, autnum


def lookup_single(ip_or_cidr: PREFIX_UNION_T):
    try:
        requested_network: PREFIX_UNION_T = CIDR(ip_or_cidr)
    except ValueError:
        return {"error": "invalid request"}, 400
    timestamp = datetime.datetime.now().timestamp()
    prefix, route, autnum = lookup_prefix(requested_network)
    with database.write_txn() as txn:
        lookedup = database.lookedup_get(txn, requested_network.compressed)
        if lookedup is None:
            lookedup = LookedUp()
            lookedup.cidr = requested_network.compressed.encode()
            lookedup.timestamp = int(timestamp)
            lookedup.count = 0
        lookedup.count += 1
        database.lookedup_put(txn, lookedup)
        if prefix is None:
            notfound_obj = database.notfound_get(txn, requested_network.compressed)
            if notfound_obj is None:
                notfound_obj = NotFound()
                notfound_obj.cidr = requested_network.compressed.encode()
                notfound_obj.timestamp = int(timestamp)
                notfound_obj.count = 0
            notfound_obj.count += 1
            database.notfound_put(txn, notfound_obj)
            return {"error": "no covering prefix found"}, 404
    result = render_prefix(prefix, autnum)
    return result
