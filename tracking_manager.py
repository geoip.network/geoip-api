from datetime import datetime, timedelta
from os import environ
from threading import Timer
from typing import List, Dict
from urllib.parse import urljoin, quote

import requests

trackingservice_auth = None


def get_bearer():
    global trackingservice_auth
    if (trackingservice_auth is None) or (
        trackingservice_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        payload = {
            "client_id": environ.get("GEOIP_INTERNAL_AUTH_ID"),
            "client_secret": environ.get("GEOIP_INTERNAL_AUTH_SECRET"),
            "audience": environ.get("GEOIP_TRACKINGSERVICE_URL"),
        }
        trackingservice_auth = requests.post(
            urljoin(environ.get("GEOIP_INTERNAL_AUTH_URL"), "v1.0/token"), json=payload
        ).json()
        trackingservice_auth["expires"] = datetime.now() + timedelta(
            seconds=trackingservice_auth["expires_in"]
        )
    return trackingservice_auth["access_token"]


def visit_token(user_id: str, key: str, target: str):
    result = requests.post(
        urljoin(
            environ.get("GEOIP_TRACKINGSERVICE_URL"),
            f"v1.0/owner/{quote(user_id)}/token/{quote(key)}/visit",
        ),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json={"target": target},
    )
