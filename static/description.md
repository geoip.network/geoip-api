<br />

## Introduction
GeoIP.Network provides accurate localisation data for IP addresses (and ranges).

The API aggregates results to the nearest /24 (256 IPs), this improves speed while not sacrificing accuracy.
We can do this because ISPs commonly allocate IP addresses in blocks of 256 to a city / region.

The quickest way to get started is to use one of the official Libraries, however if you'd like to write your own integration or are using a language that isn't get supported then this API is the next best thing.

<br />

## Authentication
If you are making less than 10k requests per day then you don't need to authenticate, but we do ask that you set your User-Agent header so that we can keep count of the number of free API users.
If you need to make more than 10k requests per day, then you will need to sign-up as a sponsor to generate an API Key.

Authentication is offered via two mechanisms Bearer Token and Session Cookie.

### Bearer token
![Screenshot of new Bearer Token form](/static/screenshots/GenerateAPIKey-Bearer.png)

To generate a token log-in to your sponsor account and navigate to the API Keys page (or via [this link](https://www.geoip.network/auth/apikeys))

Check the radio button for `Bearer Token`, set an `Application Name`, and decide whether you want `Metrics Tracking` or not, then click `Generate`.

![Screenshot of new Bearer Token modal](/static/screenshots/GenerateAPIKey-Bearer-Generated.png)

This will open a modal box that provides your new Bearer Token.

To use this token, simply provide it as an Authorization Header (as per [RFC6750](https://datatracker.ietf.org/doc/html/rfc6750#page-5))

#### For example:
```HTTP
Authorization: Bearer 1ubiWisGdxKtR9Ds19YzYO1ZQgBxkvSZ9jdXUrreblY=
```

<br />

### Cookie / Dynamic Auth
The "Dynamic" authentication method is perhaps a little over the top, but allows for reducing the number of times that "long-lived" credentials are transmitted (*Theoretically* reducing the risk of credential compromise).

![Screenshot of new Dynamic auth form](/static/screenshots/GenerateAPIKey-Cookie.png)

To generate new credentials log-in to your sponsor account and navigate to the API Keys page (or via [this link](https://www.geoip.network/auth/apikeys))

Check the radio button for `Dynamic`,  set an `Application Name`, and decide whether you want `Metrics Tracking` or not, then click `Generate`

![Screenshot of new Dynamic auth modal](/static/screenshots/GenerateAPIKey-Cookie-Generated.png)

This will open a modal box that provides your new API username and password.

To use this you will need to make a `login` request to the Authentication API ["https://auth.geoip.network/v1.0/login"](https://auth.geoip.network) providing the `username` and `password` via an `Authorization header` using the `BasicAuth` schema.

#### For example:
```HTTP
Authorization: Basic bHd2S0xLbTJoZVVrV3R5LzpVNjlUYTFGc3JMM0NQT1lP
```

This will return a `Set-Cookie` Header that defines a cookie called `GEOIP_SESSION`, a `Refresh` key, and a Time-To-Live (TTL) for the `Cookie`.

#### Refresh
When the Cookie is close to expiry it is recommended to use the `Refresh` key to renew the cookie by making a new login request.

For this login request exclude the `Authorization header` and instead provide the `Refresh` key as a Header with the name `X-RENEW-KEY`.

```HTTP
Cookie: VGhpcyBpcyBhbmQgZXhhbXBsZSBjb29raWUgbm90IGEgcmVhbCBvbmU=
X-RENEW-KEY: QSByZW5ldyBrZXkg
```

<br />

# API
