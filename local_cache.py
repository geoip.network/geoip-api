from datetime import datetime
from functools import wraps
from collections import namedtuple
from typing import Dict

CacheItem = namedtuple("CacheItem", ["time", "value"])

_cache: Dict[tuple, CacheItem] = {}
# Todo: Limit cache depth


def cache_this(f):
    global _cache
    timeout = 3600

    @wraps(f)
    def wrapper(*args, **kwargs):
        if (args[0] in _cache) and (
            (datetime.now() - _cache[args[0]].time).total_seconds() < timeout
        ):
            return _cache[args[0]].value
        value = f(*args, **kwargs)
        _cache[args[0]] = CacheItem(datetime.now(), value)
        return value

    return wrapper
