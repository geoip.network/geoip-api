def render_prefix(prefix, autnum):
    result = prefix.as_dict
    result["geo"] = {
        "type": "Feature",
        "properties": {"radius": result["radius"]},
        "geometry": {
            "type": "Point",
            "coordinates": [result["longitude"], result["latitude"]],
        },
    }
    del result["longitude"]
    del result["latitude"]
    del result["radius"]
    result["asn"] = int_to_asn(autnum.asn)
    result["as-name"] = autnum.name.decode()
    return result


def int_to_asn(asn: int):
    return f"AS{asn}"


def asn_to_int(asn: str):
    return int(asn[2:])
