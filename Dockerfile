FROM registry.gitlab.com/geoip.network/base-image:v1.1.2

RUN pip install --upgrade pip
RUN pip install poetry
ADD ./poetry.lock /flask/poetry.lock
ADD ./pyproject.toml /flask/pyproject.toml
RUN cd flask; poetry config virtualenvs.create false; poetry install
ADD ./ /flask
WORKDIR /flask

EXPOSE 8080

CMD poetry run gunicorn app:app -c gunicorn_conf.py