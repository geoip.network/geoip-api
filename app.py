"""
VERSION: 1.0.0
LICENSE: BSD-3-Clause
COPYRIGHT: Plaintextnerds Ltd. - 2021
"""
import datetime
import ipaddress
import re
from collections import defaultdict
from functools import wraps
from os import environ, path
from typing import Union, Dict, List
from urllib.parse import urljoin

import newrelic.agent
import requests
import nacl.exceptions
from flask import Flask, request, jsonify, _request_ctx_stack, render_template, send_from_directory
from flask_cors import cross_origin
from flask_limiter import Limiter
from flask_limiter.util import get_ipaddr
from jose import jwt
from nacl.encoding import URLSafeBase64Encoder
from nacl.signing import VerifyKey

import tracking_manager
from authentication import AuthMiddleware, Role, protect, AuthError
from database import open_database
from lookup import lookup_single, lookup_bulk
from render_helpers import asn_to_int, render_prefix
from tracking_manager import visit_token
from models import Prefix, Route, AutNum

if path.isfile("./newrelic.ini"):
    newrelic.agent.initialize("./newrelic.ini")
app = Flask(__name__)
limiter = Limiter(
    app,
    key_func=get_ipaddr,
    headers_enabled=True,
    default_limits=["10000 per day", "500 per hour"],
)
auth_middleware = AuthMiddleware(app.wsgi_app)
app.wsgi_app = auth_middleware

database = open_database()
tracking_manager.get_bearer()

M2MAUTH_DOMAIN = environ.get("GEOIP_INTERNAL_AUTH_URL")
API_AUDIENCE = environ.get("GEOIP_URL")
ALGORITHMS = ["ED25519"]

CERTS = requests.get(urljoin(M2MAUTH_DOMAIN, "/v1.0/certs")).json()  # nosemgrep
KEYS = {
    int(key): {
        "key": VerifyKey(val["public_key"], encoder=URLSafeBase64Encoder),
        "issuer": val["issuer"],
        "refresh": val["refresh"],
    }
    for key, val in CERTS.items()
}


@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response


# Format error response and append status code
def get_token_auth_header():
    """
    Obtains the Access Token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError(
            {
                "code": "authorization_header_missing",
                "description": "Authorization header is expected",
            },
            401,
        )

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must start with" " Bearer",
            },
            401,
        )
    elif len(parts) == 1:
        raise AuthError(
            {"code": "invalid_header", "description": "Token not found"}, 401
        )
    elif len(parts) > 2:
        raise AuthError(
            {
                "code": "invalid_header",
                "description": "Authorization header must be a valid Bearer token",
            },
            401,
        )

    token = parts[1]
    return token


def requires_auth(f):
    """Determines if the Access Token is valid"""

    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_token_auth_header()
        unverified_header = jwt.get_unverified_header(token)
        key = KEYS.get(unverified_header["kid"])
        if key is not None:
            try:
                chunks = token.split(".")
                signature = URLSafeBase64Encoder.decode(chunks[2].encode())
                key["key"].verify(".".join(chunks[:2]).encode(), signature)
            except nacl.exceptions.BadSignatureError as e:
                raise AuthError(
                    {
                        "code": "invalid_signature",
                        "description": "Signature could not be verified",
                    },
                    401,
                ) from e
            claims = jwt.get_unverified_claims(token)
            try:
                if (
                    datetime.datetime.fromtimestamp(claims["exp"])
                    < datetime.datetime.now()
                ):
                    raise AuthError(
                        {"code": "token_expired", "description": "Token is expired"},
                        401,
                    )
                r = re.compile(r"(https?://)?" + re.escape(API_AUDIENCE) + "/?")
                if (r.match(claims["aud"]) is None) and (
                    claims["iss"] != M2MAUTH_DOMAIN
                ):
                    raise AuthError(
                        {
                            "code": "invalid_claims",
                            "description": "Incorrect claims,"
                            "please check the audience and issuer",
                        },
                        401,
                    )
            except KeyError as e:
                raise AuthError(
                    {
                        "code": "invalid_header",
                        "description": "Unable to parse authentication token.",
                    },
                    401,
                ) from e
            _request_ctx_stack.top.current_user = claims
            return f(*args, **kwargs)
        raise AuthError(
            {"code": "invalid_header", "description": "Unable to find appropriate key"},
            401,
        )

    return decorated


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    token = get_token_auth_header()
    unverified_claims = jwt.get_unverified_claims(token)
    if unverified_claims.get("scope"):
        token_scopes = unverified_claims["scope"].split()
        for token_scope in token_scopes:
            if token_scope == required_scope:
                return True
    return False


@app.route("/", methods=["GET"])
@cross_origin(origins=["*"], methods=["GET"], max_age=3600)
def index():
    return render_template("index.html", url=environ.get("GEOIP_URL"))


@app.route("/swagger/v1.0", methods=["GET"])
@cross_origin(origins=["*"], methods=["GET"], max_age=3600)
def swagger_v1_0():
    return send_from_directory('static', 'OpenAPI.yaml')


@app.route("/v1.0/cidrs", methods=["POST"])
@protect(Role.user, limiter, enable_exempt=True)
def lookup_cidrs():
    bulk = request.json
    if not isinstance(bulk, list):
        return {"error": "invalid request"}, 400
    bulk = (str(item) for item in bulk)
    responses = lookup_bulk(bulk)
    return jsonify(responses)


@app.route("/v1.0/cidrs/internal", methods=["POST"])
@requires_auth
@limiter.exempt
def lookup_cidrs_internal():
    bulk = request.json
    if not requires_scope("read:cidr"):
        return {
                   "code": "missing_scope",
                   "description": "No matching scope was found",
                    }, 401
    if not isinstance(bulk, list):
        return {
                   "code": "invalid_request",
                   "description": "The submitted object was not of type `list`",
                    }, 400
    bulk = (str(item) for item in bulk)
    responses = lookup_bulk(bulk)
    return jsonify(responses)


@app.route("/v1.0/cidr/<path:ip_or_cidr>", methods=["GET"])
@cross_origin(origins=["*"], methods=["GET"], max_age=3600)
@protect(Role.user, limiter, enable_exempt=True)
def lookup_cidr(ip_or_cidr):
    try:
        requested_network: Union[
            ipaddress.IPv4Network, ipaddress.IPv6Network
        ] = ipaddress.ip_network(ip_or_cidr, strict=False)
    except ValueError:
        return {"error": "invalid request"}, 400
    if request.environ.get("geoip_user_authid") is not None:
        visit_token(
            request.environ["geoip_user_authid"],
            request.environ["geoip_user_app"],
            requested_network.compressed,
        )
    return lookup_single(ip_or_cidr)


@app.route("/v1.0/cidr/<path:ip_or_cidr>/internal", methods=["GET"])
@requires_auth
@limiter.exempt
def lookup_cidr_internal(ip_or_cidr):
    if not requires_scope("read:cidr"):
        return {}, 401
    return lookup_single(ip_or_cidr)


@app.route("/v1.0/metrics", methods=["GET"])
@requires_auth
@limiter.exempt
def metrics():
    if requires_scope("read:metrics"):
        jdata: Dict[str, Union[Dict, List]] = defaultdict(dict)
        with database.read_txn() as txn:
            notfound_list = database.notfound_all(txn)
            lookedup_list = database.lookedup_all(txn)
        jdata["metrics"]["notfound_count"] = len(notfound_list)
        jdata["metrics"]["lookedup_count"] = len(lookedup_list)
        jdata["notfound"] = [notfound.as_dict for notfound in notfound_list]
        jdata["lookedup"] = [lookedup.as_dict for lookedup in lookedup_list]
        return jdata, 200
    return {}, 401


def store_prefix_input(jdata, requested_network):
    try:
        new_prefix, new_route, new_autnum = parse_prefix_input(jdata, requested_network)
        with database.write_txn() as txn:
            database.prefix_put(txn, new_prefix)
            database.route_put(txn, new_route)
            database.autnum_put(txn, new_autnum)
            result = render_prefix(new_prefix, new_autnum), 201
    except ValueError as e:
        if hasattr(e, "message"):
            result = {"error": e.message}, 400
        else:
            result = {"error": str(e)}, 500
    return result


def parse_prefix_input(jdata, requested_network):
    timestamp = datetime.datetime.now().timestamp()
    new_prefix = Prefix()
    new_route = Route()
    new_autnum = AutNum()
    new_prefix.timestamp = new_route.timestamp = new_autnum.timestamp = int(timestamp)
    new_prefix.cidr = new_route.cidr = requested_network.compressed.encode()
    try:
        if jdata["cidr"] != requested_network.compressed:
            raise ValueError("prefix mismatch - request and path do not match")
        if jdata["rir"] not in ("RIPE", "APNIC", "AFRINIC", "ARIN", "LACNIC"):
            raise ValueError("invalid RIR")
        if not str(jdata["asn"]).startswith("AS") and str(jdata["asn"])[2:].isdigit():
            raise ValueError("unsupported AS number format")
        if len(str(jdata["as-name"])) > 128:
            raise ValueError("as-name too long")
        new_prefix.longitude = float(jdata["geo"]["geometry"]["coordinates"][0])
        new_prefix.latitude = float(jdata["geo"]["geometry"]["coordinates"][1])
        new_prefix.radius = float(jdata["geo"]["properties"].get("radius", 0))
        new_prefix.rir = jdata["rir"].encode()
        new_prefix.allocated_cc = jdata["allocated_cc"].encode()
        new_autnum.asn = new_route.asn = asn_to_int(jdata["asn"])
        new_autnum.name = jdata["as-name"].encode()
    except (IndexError, KeyError):
        raise ValueError("missing field")
    return new_prefix, new_route, new_autnum


def main():
    global app
    app.run()


if __name__ == "__main__":
    main()
