import base64
import json
from datetime import datetime
from enum import IntEnum
from functools import wraps
from os import environ, path
from typing import Optional

import nacl.exceptions
import nacl.pwhash
import nacl.secret
import nacl.signing
import nacl.utils
from flask import request
from requests import HTTPError, ConnectionError
from werkzeug.exceptions import abort
from werkzeug.wrappers import Request

from auth_service import validate_bearer, get_bearer, validate_session


# Error handler
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


class Role(IntEnum):
    user = 0
    sponsor = 10
    isp = 25
    service = 50
    admin = 100


def protect(role, limiter, enable_exempt=False):
    def decorator(func):
        @wraps(func)
        def _(*args, **kwargs):
            if role <= request.environ["geoip_user_role"]:
                if enable_exempt and request.environ["geoip_user_role"] >= Role.sponsor:
                    response = limiter.exempt(func)(*args, **kwargs)
                else:
                    response = func(*args, **kwargs)
                if request.environ["geoip_user_provided"]:
                    if isinstance(response, tuple):
                        return (
                            response[0],
                            request.environ["geoip_user_response"]
                            if 200 <= response[1] < 300
                            else response[1],
                        )
                    else:
                        return response, request.environ["geoip_user_response"]
                else:
                    return response
            else:
                abort(401)

        return _

    return decorator


class AuthMiddleware:
    """
    Authentication WSGI middleware
    """

    def __init__(self, app):
        self.app = app
        self.sk: Optional[nacl.signing.VerifyKey] = None
        self.ek: Optional[bytes] = None
        self.auth_url: str = environ.get("GEOIP_AUTHSERVICE_URL")
        get_bearer()
        if path.isfile("secrets/ed25519.pub"):
            with open("secrets/ed25519.pub", "rb") as f:
                self.sk = nacl.signing.VerifyKey(f.read())
        else:
            raise RuntimeError("can't find signing key")
        if path.isfile("secrets/encrypt.key"):
            with open("secrets/encrypt.key", "rb") as f:
                self.ek = f.read()
        else:
            raise RuntimeError("can't find encryption key")

    def __call__(self, env, start_response):
        request = Request(env)
        enc_signature = request.cookies.get("GEOIP_SESSION")
        role = None
        provided = False
        if enc_signature:
            provided = True
            try:
                role = self.verify_jwt(enc_signature)
                if not self.spotcheck(enc_signature):
                    role = None
            except (ValueError, HTTPError, ConnectionError):
                pass
        else:
            if (
                auth := request.headers.get("authorization")
            ) and auth.lower().startswith("bearer"):
                provided = True
                try:
                    enc_jwt = validate_bearer(auth.split(" ")[1])
                    role, auth_id, application_name = self.verify_jwt(enc_jwt)
                    if role:
                        env["geoip_user_app"] = application_name
                        env["geoip_user_authid"] = auth_id
                    else:
                        env["geoip_user_app"] = None
                        env["geoip_user_authid"] = None
                except (ValueError, HTTPError, ConnectionError):
                    pass
        env["geoip_user_provided"] = provided
        if role:
            env["geoip_user_role"] = role
            env["geoip_user_response"] = 200
        else:
            env["geoip_user_role"] = Role.user
            env["geoip_user_response"] = 401
        return self.app(env, start_response)

    def verify_jwt(self, enc_signature):
        sealed = nacl.secret.SecretBox(self.ek)
        try:
            signature = sealed.decrypt(
                enc_signature.encode(),
                encoder=nacl.secret.encoding.URLSafeBase64Encoder,
            )
        except nacl.exceptions.CryptoError:
            raise ValueError("unable to decrypt")
        jwt = self.sk.verify(signature)
        header_b64, payload_b64 = jwt.split(b".")
        header = json.loads(base64.b64decode(header_b64).decode())
        payload = json.loads(base64.b64decode(payload_b64).decode())
        if header.get("alg") != "ED25519":
            raise ValueError("invalid header")
        if (
            payload.get("timestamp", 0) + payload.get("ttl", 0)
            <= datetime.now().timestamp()
        ):
            raise ValueError("expired")
        return payload["role"], payload["auth_id"], payload["application_name"]

    def spotcheck(self, enc_signature: str) -> bool:
        try:
            validate_session(enc_signature)
        except HTTPError as e:
            return False
        return True
