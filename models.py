"""
VERSION: 1.0.0
LICENSE: BSD-3-Clause
COPYRIGHT: Plaintextnerds Ltd. - 2021
"""

from ctypes import Structure, c_char, c_double, c_uint64


class AsDictMixin:
    @property
    def as_dict(self):
        d = {}
        for (key, _) in self._fields_:
            if isinstance(getattr(self, key), AsDictMixin):
                d[key] = getattr(self, key).as_dict
            elif isinstance(getattr(self, key), bytes):
                d[key] = getattr(self, key).decode()
            else:
                d[key] = getattr(self, key)
        return d

    def __repr__(self):
        return f"{self.__class__.__name__}({', '.join(['='.join([key, str(val)]) for key, val in self.as_dict.items()])})"


class NotFound(Structure, AsDictMixin):
    _fields_ = [
        ("cidr", c_char * 50),
        ("count", c_uint64),
        ("timestamp", c_uint64),
    ]


class LookedUp(Structure, AsDictMixin):
    _fields_ = [
        ("cidr", c_char * 50),
        ("count", c_uint64),
        ("timestamp", c_uint64),
    ]


class Prefix(Structure, AsDictMixin):
    _fields_ = [
        ("cidr", c_char * 50),
        ("longitude", c_double),
        ("latitude", c_double),
        ("radius", c_double),
        ("rir", c_char * 17),
        ("allocated_cc", c_char * 3),
        ("timestamp", c_uint64),
    ]


class Route(Structure, AsDictMixin):
    _fields_ = [
        ("cidr", c_char * 50),
        ("asn", c_uint64),
        ("timestamp", c_uint64),
    ]


class AutNum(Structure, AsDictMixin):
    _fields_ = [
        ("asn", c_uint64),
        ("name", c_char * 129),
        ("timestamp", c_uint64),
    ]
