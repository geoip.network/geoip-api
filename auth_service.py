from datetime import datetime, timedelta
from os import environ
from urllib.parse import urljoin
from base64 import urlsafe_b64encode

import requests

from local_cache import cache_this

management_auth = None


def get_bearer():
    global management_auth
    if (management_auth is None) or (
        management_auth["expires"] - datetime.now() < timedelta(seconds=0)
    ):
        payload = {
            "grant_type": "client_credentials",
            "client_id": environ.get("GEOIP_INTERNAL_AUTH_ID"),
            "client_secret": environ.get("GEOIP_INTERNAL_AUTH_SECRET"),
            "audience": environ.get("GEOIP_AUTHSERVICE_URL"),
        }
        management_auth = requests.post(
            urljoin(environ.get("GEOIP_INTERNAL_AUTH_URL"), "v1.0/token"), json=payload
        ).json()
        management_auth["expires"] = datetime.now() + timedelta(
            seconds=management_auth["expires_in"]
        )
    return management_auth["access_token"]


@cache_this
def validate_session(payload):
    result = requests.post(
        urljoin(environ.get("GEOIP_AUTHSERVICE_URL"), "v1.0/validate_session"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json={"session": payload},
    )
    result.raise_for_status()


@cache_this
def validate_bearer(payload):
    result = requests.post(
        urljoin(environ.get("GEOIP_AUTHSERVICE_URL"), "v1.0/validate_bearer"),
        headers={"Authorization": f"Bearer {get_bearer()}"},
        json={"bearer": urlsafe_b64encode(payload.encode()).decode()},
    )
    result.raise_for_status()
    return result.text
