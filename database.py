from typing import List

import lmdb  # type: ignore
from models import Prefix, Route, AutNum, NotFound, LookedUp

Ki = 1024
Mi = 1024 * Ki
Gi = 1024 * Mi
MILLISECONDS = 1000  # Convert from seconds to milliseconds

database: "Database" = None


class Database:
    def __init__(self):
        self.env = lmdb.Environment(
            "../database.lmdb",
            map_size=8 * Gi,
            subdir=True,
            readonly=False,
            metasync=True,
            sync=True,
            map_async=False,
            mode=493,
            create=True,
            readahead=True,
            writemap=False,
            meminit=True,
            max_readers=16,
            max_dbs=32,
            max_spare_txns=1,
            lock=True,
        )
        self.notfound_db = self.env.open_db("notfound".encode())
        self.lookedup_db = self.env.open_db("lookedup".encode())
        self.prefix_db = self.env.open_db("prefix".encode())
        self.prefix_db = self.env.open_db("prefix".encode())
        self.route_db = self.env.open_db("route".encode())
        self.route_asn_index = self.env.open_db(
            "route_asn_index".encode(), dupsort=True
        )
        self.autnum_db = self.env.open_db("autnum".encode())

    def write_txn(self):
        return self.env.begin(write=True)

    def read_txn(self):
        return self.env.begin()

    def notfound_all(self, txn) -> List[NotFound]:
        output: List[NotFound] = []
        with txn.cursor(self.notfound_db) as cursor:
            for _, value in cursor:
                notfound = NotFound.from_buffer_copy(value)
                output.append(notfound)
        return output

    def notfound_put(self, txn, notfound: NotFound):
        txn.put(notfound.cidr, bytes(notfound), db=self.notfound_db)

    def notfound_get(self, txn, cidr: str) -> NotFound:
        with txn.cursor(self.notfound_db) as cursor:
            if cursor.set_key(cidr.encode()):
                notfound = NotFound.from_buffer_copy(cursor.value())
                return notfound
        return None

    def lookedup_all(self, txn) -> List[LookedUp]:
        output: List[LookedUp] = []
        with txn.cursor(self.lookedup_db) as cursor:
            for _, value in cursor:
                lookedup = LookedUp.from_buffer_copy(value)
                output.append(lookedup)
        return output

    def lookedup_put(self, txn, lookedup: LookedUp):
        txn.put(lookedup.cidr, bytes(lookedup), db=self.lookedup_db)

    def lookedup_get(self, txn, cidr: str) -> LookedUp:
        with txn.cursor(self.lookedup_db) as cursor:
            if cursor.set_key(cidr.encode()):
                lookedup = LookedUp.from_buffer_copy(cursor.value())
                return lookedup
        return None

    def prefix_put(self, txn, prefix: Prefix):
        txn.put(prefix.cidr, bytes(prefix), db=self.prefix_db)

    def prefix_get(self, txn, cidr: str) -> Prefix:
        with txn.cursor(self.prefix_db) as cursor:
            return self.prefix_get_bulk(cursor, cidr)

    def prefix_get_bulk(self, cursor, cidr: str) -> Prefix:
        if cursor.set_key(cidr.encode()):
            prefix = Prefix.from_buffer_copy(cursor.value())
            if prefix.radius == 0.0:
                prefix.radius = -1.0
            return prefix
        return None

    def route_put(self, txn, route: Route):
        txn.put(route.cidr, bytes(route), db=self.route_db)
        txn.put(str(route.asn).encode(), route.cidr, db=self.route_asn_index)

    def route_get_by_prefix(self, txn, cidr: str) -> Route:
        with txn.cursor(self.route_db) as cursor:
            return self.route_get_by_prefix_bulk(cursor, cidr)
        return None

    def route_get_by_prefix_bulk(self, cursor, cidr: str) -> Route:
        if cursor.set_key(cidr.encode()):
            route = Route.from_buffer_copy(cursor.value())
            return route

    def route_get_by_asn(self, txn, asn: int) -> List[Route]:
        routes = []
        with txn.cursor(self.route_asn_index) as cursor:
            if cursor.set_key(str(asn).encode()):
                for cidr in cursor.iternext_dup():
                    routes.append(self.route_get_by_prefix(txn, cidr))
        return routes

    def autnum_put(self, txn, autnum: AutNum):
        txn.put(str(autnum.asn).encode(), bytes(autnum), db=self.autnum_db)

    def autnum_get(self, txn, asn: int) -> AutNum:
        with txn.cursor(self.autnum_db) as cursor:
            return self.autnum_get_bulk(cursor, asn)
        return None

    def autnum_get_bulk(self, cursor, asn: int) -> AutNum:
        if cursor.set_key(str(asn).encode()):
            autnum = AutNum.from_buffer_copy(cursor.value())
            return autnum


def open_database():
    global database
    if database is None:
        database = Database()
    return database
